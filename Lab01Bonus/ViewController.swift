//
//  ViewController.swift
//  Lab01Bonus
//
//  Created by User on 5/9/2019.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var firstVal = true
    var currentModes : currentMode?
    var currentOperater : operatorType?
    var subTotal:String?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        calDisplay.text = "0"
        currentModes = currentMode.dec
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var currentDisplay: UILabel!
    @IBOutlet weak var calDisplay: UITextField!

    @IBAction func clearCal(_ sender: UIButton) {
        calDisplay.text = "0"
        subTotal = nil
        currentOperater = nil
        currentModes = currentMode.dec
        currentDisplay.text = "Current: Dec"
        firstVal = true
    }
    @IBAction func pressOne(_ sender: UIButton) {
        if firstVal{
            calDisplay.text = "1"
            firstVal = false
        }else{
            calDisplay.text! += "1"
        }
    }
    @IBAction func pressTwo(_ sender: UIButton) {
        if firstVal , currentModes != currentMode.bin{
            calDisplay.text = "2"
            firstVal = false
        }else if currentModes != currentMode.bin{
            calDisplay.text! += "2"
        }
    }
    @IBAction func pressThree(_ sender: UIButton) {
        if firstVal , currentModes != currentMode.bin{
            calDisplay.text = "3"
            firstVal = false
        }else if currentModes != currentMode.bin{
            calDisplay.text! += "3"
        }
    }
    @IBAction func pressFour(_ sender: UIButton) {
        if firstVal , currentModes != currentMode.bin{
            calDisplay.text = "4"
            firstVal = false
        }else if currentModes != currentMode.bin{
            calDisplay.text! += "4"
        }
    }
    @IBAction func pressFive(_ sender: UIButton) {
        if firstVal , currentModes != currentMode.bin{
            calDisplay.text = "5"
            firstVal = false
        }else if currentModes != currentMode.bin{
            calDisplay.text! += "5"
        }
    }
    @IBAction func pressSix(_ sender: UIButton) {
        if firstVal , currentModes != currentMode.bin{
            calDisplay.text = "6"
            firstVal = false
        }else if currentModes != currentMode.bin{
            calDisplay.text! += "6"
        }
    }
    @IBAction func pressSeven(_ sender: UIButton) {
        if firstVal , currentModes != currentMode.bin{
            calDisplay.text = "7"
            firstVal = false
        }else if currentModes != currentMode.bin{
            calDisplay.text! += "7"
        }
    }
    @IBAction func pressEight(_ sender: UIButton) {
        if firstVal , currentModes != currentMode.bin, currentModes != currentMode.oct{
            calDisplay.text = "8"
            firstVal = false
        }else if currentModes != currentMode.bin, currentModes != currentMode.oct{
            calDisplay.text! += "8"
        }
    }
    @IBAction func pressNine(_ sender: UIButton) {
        if firstVal, currentModes != currentMode.bin, currentModes != currentMode.oct{
            calDisplay.text = "9"
            firstVal = false
        }else if currentModes != currentMode.bin, currentModes != currentMode.oct{
            calDisplay.text! += "9"
        }
    }
    @IBAction func pressZero(_ sender: UIButton) {
        if firstVal {
            calDisplay.text = "0"
            firstVal = false
        }else{
            calDisplay.text! += "0"
        }
    }
    @IBAction func pressA(_ sender: Any) {
        if firstVal, currentModes == currentMode.hex {
            calDisplay.text = "a"
            firstVal = false
        }else if currentModes == currentMode.hex{
            calDisplay.text! += "a"
        }
    }
    @IBAction func pressB(_ sender: Any) {
        if firstVal , currentModes == currentMode.hex{
            calDisplay.text = "b"
            firstVal = false
        }else if currentModes == currentMode.hex{
            calDisplay.text! += "b"
        }
    }
    @IBAction func pressC(_ sender: Any) {
        if firstVal , currentModes == currentMode.hex{
            calDisplay.text = "c"
            firstVal = false
        }else if currentModes == currentMode.hex{
            calDisplay.text! += "c"
        }
    }
    @IBAction func pressD(_ sender: Any) {
        if firstVal , currentModes == currentMode.hex{
            calDisplay.text = "d"
            firstVal = false
        }else if currentModes == currentMode.hex{
            calDisplay.text! += "d"
        }
    }
    @IBAction func pressE(_ sender: Any) {
        if firstVal , currentModes == currentMode.hex{
            calDisplay.text = "e"
            firstVal = false
        }else if currentModes == currentMode.hex{
            calDisplay.text! += "e"
        }
    }
    @IBAction func pressF(_ sender: Any) {
        if firstVal , currentModes == currentMode.hex{
            calDisplay.text = "f"
            firstVal = false
        }else if currentModes == currentMode.hex{
            calDisplay.text! += "f"
        }
    }
    
    enum operatorType {
        case add
        case sub
        case mul
        case div
    }
    
    enum currentMode {
        case bin
        case oct
        case dec
        case hex
    }
    
    @IBAction func pressAdd(_ sender: UIButton) {
        if let currentSubTotal = subTotal {
            subTotal = calAnswer(subTotal: currentSubTotal, currentInput: calDisplay.text!)
        } else {
            subTotal = calDisplay.text!
        }
        currentOperater = operatorType.add
        firstVal = true
        calDisplay.text = "\(subTotal!)"
    }
    
    @IBAction func pressSub(_ sender: UIButton) {
        if let currentSubTotal = subTotal {
            subTotal = calAnswer(subTotal: currentSubTotal, currentInput: calDisplay.text!)
        } else {
            subTotal = calDisplay.text!
        }
        currentOperater = operatorType.sub
        firstVal = true
        calDisplay.text = "\(subTotal!)"
    }
    @IBAction func pressMul(_ sender: UIButton) {
        if let currentSubTotal = subTotal {
            subTotal = calAnswer(subTotal: currentSubTotal, currentInput: calDisplay.text!)
        } else {
            subTotal = calDisplay.text!
        }
        currentOperater = operatorType.mul
        firstVal = true
        calDisplay.text = "\(subTotal!)"
    }
    @IBAction func pressDiv(_ sender: UIButton) {
        if let currentSubTotal = subTotal {
            subTotal = calAnswer(subTotal: currentSubTotal, currentInput: calDisplay.text!)
        } else {
            subTotal = calDisplay.text!
        }
        currentOperater = operatorType.div
        firstVal = true
        calDisplay.text = "\(subTotal!)"
    }
    
    @IBAction func pressEql(_ sender: UIButton) {
        var result: String?
        if subTotal == nil{
            subTotal = "0"
        }
        result = calAnswer(subTotal: subTotal!, currentInput: calDisplay.text!)
        calDisplay.text = "\(result!)"
        subTotal = nil
        firstVal = true
    }
    
    func calAnswer(subTotal: String, currentInput: String) -> String{
        var result: String?
        var tmpanswer: Int?
        var decSubTotal: Int?
        var decCurrentInput: Int?
        
        if let typeOfString = currentModes {
            switch(typeOfString) {
            case .bin:
                decSubTotal = Int(String(Int(strtoul(subTotal, nil, 2)) , radix: 10))
                decCurrentInput = Int(String(Int(strtoul(currentInput, nil, 2)) , radix: 10))
            case .oct:
                decSubTotal = Int(String(Int(strtoul(subTotal, nil, 8)) , radix: 10))
                decCurrentInput = Int(String(Int(strtoul(currentInput, nil, 8)) , radix: 10))
            case .dec:
                decSubTotal = Int(subTotal)
                decCurrentInput = Int(currentInput)
            case .hex:
                decSubTotal = Int(String(Int(strtoul(subTotal, nil, 16)) , radix: 10))
                decCurrentInput = Int(String(Int(strtoul(currentInput, nil, 16)) , radix: 10))
            }
        }
        if let finalOperator = currentOperater {
            switch(finalOperator) {
            case .add :
                tmpanswer = decSubTotal! + decCurrentInput!
            case .sub :
                tmpanswer = decSubTotal! - decCurrentInput!
            case .mul :
                tmpanswer = decSubTotal! * decCurrentInput!
            case .div :
                tmpanswer = decSubTotal! / decCurrentInput!
            }
        }
        if let typeOfString = currentModes {
            switch(typeOfString) {
            case .bin:
                result = String(tmpanswer! , radix: 2)
            case .oct:
                result = String(tmpanswer! , radix: 8)
            case .dec:
                result = String(tmpanswer!)
            case .hex:
                result = String(tmpanswer! , radix: 16)
            }
        }
        
        return result!
    }
    
    @IBAction func ToBin(_ sender: Any) {
        calDisplay.text = changeType(changeToMode: currentMode.bin, mode: 2)
        currentDisplay.text = "Current: Bin"
    }
    
    @IBAction func ToOct(_ sender: Any) {
        calDisplay.text = changeType(changeToMode: currentMode.oct, mode: 8)
        currentDisplay.text = "Current: Oct"
    }
    
    @IBAction func ToDec(_ sender: Any) {
        calDisplay.text = changeType(changeToMode: currentMode.dec, mode: 10)
        currentDisplay.text = "Current: Dec"
    }
    @IBAction func ToHex(_ sender: Any) {
        calDisplay.text = changeType(changeToMode: currentMode.hex, mode: 16)
        currentDisplay.text = "Current: Hex"
    }
    
    func changeType(changeToMode: currentMode, mode: Int) -> String{
        var result : String?
        if let typeOfString = currentModes {
            switch(typeOfString) {
            case .bin:
                currentModes = changeToMode
                result = String(Int(strtoul(calDisplay.text!, nil, 2)) , radix: mode)
            case .oct:
                currentModes = changeToMode
                result = String(Int(strtoul(calDisplay.text!, nil, 8)) , radix: mode)
            case .dec:
                currentModes = changeToMode
                result = String(Int(calDisplay.text!) ?? 0, radix: mode)
            case .hex:
                currentModes = changeToMode
                result = String(Int(strtoul(calDisplay.text!, nil, 16)) , radix: mode)
            }
        }
        return result!
    }
    
}

